package hc.advocate.base;

import org.openqa.selenium.WebDriver;

import hc.advocate.pages.carePlansTab.CarePlanPage;


public class TopMenu {
	
	//TopMenu is not a Page, Pages have a TopMenu
	
	WebDriver driver;
	
	public TopMenu(WebDriver driver) {
		
		this.driver = driver;
		
	}

	public CarePlanPage goToCarePlans() {
		
		Page.click("carePlansTab_CSS");
		
		return new CarePlanPage();

	}
	
	public void goToChats() {
		
		Page.click("chatsTab_CSS");
		
	}
	
	public void goToScheduling() {
		
		Page.click("schedulingTab_CSS");
		
	}
	
	public void goToCarePlanTemplates() {
		
		Page.click("carePlanTemplateTab_CSS");
		
	}
	
	public void goToDoctors() {
		
		Page.click("doctorsTab_CSS");
		
	}
	
	public void goToFacilities() {
		
		Page.click("facilitiesTab_CSS");
		
	}
	
	public void goToMembers() {
		
		Page.click("membersTab_CSS");
		
	}
	
	public void goToBillAdvocacy() {
		
		Page.click("billAdvocacyTab_CSS");
		
	}
	
	public void goToPlans() {
		
		Page.click("plansTab_CSS");
		
	}
	
	public void goToTickets() {
		
		Page.click("ticketsTab_CSS");
		
	}
	
	public void goToMetabase() {
		
		Page.click("metabaseTab_CSS");
		
	}
	
	public void goToSignOut() {
		
		Page.click("signOutBtn_CSS");
		
	}
	

}