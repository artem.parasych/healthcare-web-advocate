package hc.advocate.pages;

import hc.advocate.base.Page;
import hc.advocate.pages.carePlansTab.CarePlanPage;

public class SignUpPage extends Page {
	
	public CarePlanPage processLogIn(String email, String password) {
		
		
	    clear("emailStringSUP_CSS");
		enterEmail(email);
		 clear("passwordStringSUP_CSS");
		enterPassword(password);
		tapLogInBtn();
		
		return new CarePlanPage();
		
	}
	
	public void enterEmail(String email) {
		
		type("emailStringSUP_CSS", email);
		
	}
	
	public void enterPassword(String password) {
		
		type("passwordStringSUP_CSS", password);
		
	}
	
	public void tapLogInBtn() {
		
		click("logInBtnSUP_CSS");
		
	}
	
	public void tapOkErrorBtn() {
		
		click("errorOkBtn_CSS");
		
	}

}
