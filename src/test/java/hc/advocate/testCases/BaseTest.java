package hc.advocate.testCases;

import org.testng.annotations.AfterSuite;

import hc.advocate.base.Page;

public class BaseTest extends Page {
	
	@AfterSuite
	public void tearDown() {
		
		quit();
		
	}

}
