package hc.advocate.testCases;

import org.testng.annotations.Test;

import hc.advocate.base.Page;
import hc.advocate.base.TopMenu;

public class SignOutTest extends BaseTest{
	
	@Test
	public void signOutBtnTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToSignOut();
		
		isElementPresent("emailStringSUP_CSS");
		
	}

}
