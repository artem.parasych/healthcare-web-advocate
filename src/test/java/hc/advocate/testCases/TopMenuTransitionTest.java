package hc.advocate.testCases;

import java.util.ArrayList;

import org.testng.annotations.Test;

import hc.advocate.base.Page;
import hc.advocate.base.TopMenu;

public class TopMenuTransitionTest extends BaseTest {
	
	@Test
	public void carePlanTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToCarePlans();
		
		isElementPresent("searchStringCPP_CSS");
		isElementPresent("organizationDropdownMenuCPP_CSS");
		isElementPresent("createNewCarePlanBtnCPP_CSS");
		isElementPresent("contentTableBodyCPP_CSS");
		
	}
	
	@Test
	public void chatsTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToChats();
		
		isElementPresent("searchStringCP_CSS");
		isElementPresent("organizationDropdownMenuCP_CSS");
		isElementPresent("statusFilterCP_XPATH");
		isElementPresent("contanteTableHeaderCP_CSS");
		
	}
	
	@Test
	public void SchedulingTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToScheduling();
		
		isElementPresent("searchStringSP_CSS");
		isElementPresent("organizationDropdownMenuSP_CSS");
		isElementPresent("contanteTableHeaderSP_CSS");
		
	}
	
	@Test
	public void CarePlanTemplatesTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToCarePlanTemplates();
		
		isElementPresent("searchStringCPTP_CSS");
		isElementPresent("addTemplateBtnCPTP_CSS");
		isElementPresent("contanteTableHeaderCPTP_CSS");
		
	}
	
	@Test
	public void DoctorsTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToDoctors();
		
		isElementPresent("searchStringDP_CSS");
		isElementPresent("addDoctorBtnDP_CSS");
		isElementPresent("contanteTableHeaderDP_CSS");
		
	}
	
	@Test
	public void FacilitiesTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToFacilities();
		
		isElementPresent("searchStringFP_CSS");
		isElementPresent("addFacilityBtnFP_CSS");
		isElementPresent("contanteTableHeaderFP_CSS");
		
	}
	
	@Test
	public void MembersTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToMembers();
		
		isElementPresent("searchStringMP_CSS");
		isElementPresent("organizationDropdownMenuMP_CSS");
		isElementPresent("showOnlyRegisteredCheckboxMP_XPATH");
		isElementPresent("contanteTableHeaderMP_CSS");
		
	}
	
	@Test
	public void BillAdvocacyTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToBillAdvocacy();
		
		isElementPresent("organizationDropdownMenuBAP_CSS");
		isElementPresent("contanteTableHeaderBAP_CSS");
		
	}
	
	@Test
	public void PlansTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToPlans();
		
		isElementPresent("searchStringPP_CSS");
		isElementPresent("organizationDropdownMenuPP_CSS");
		isElementPresent("addNePlanBtnPP_CSS");
		isElementPresent("contanteTableHeaderPP_CSS");
		
	}
	
	@Test
	public void TicketsTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToTickets();
		
		isElementPresent("sortByFilterTP_CSS");
		isElementPresent("createTicketBtnTP_CSS");
		
	}
	
	@Test
	public void MetabaseTabTest() {
		
		TopMenu tp = new TopMenu(Page.driver);
		tp.goToMetabase();
		
        String oldTab = driver.getWindowHandle();
		
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		newTab.remove(oldTab);
		driver.switchTo().window(newTab.get(0));
		
		isElementPresent("emailAddressStringMP_CSS");
		isElementPresent("passwordStringMP_CSS");
		
		driver.close();
		driver.switchTo().window(oldTab);
		
		Page.waitTillVisible("signOutBtn_CSS");
		
		
	}
	
	

}