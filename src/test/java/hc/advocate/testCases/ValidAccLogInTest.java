package hc.advocate.testCases;

import java.util.Hashtable;

import org.testng.annotations.Test;

import hc.advocate.base.Page;
import hc.advocate.base.TopMenu;
import hc.advocate.pages.SignUpPage;
import hc.advocate.utilities.Utilities;

public class ValidAccLogInTest extends BaseTest {
	
	@Test(dataProviderClass = Utilities.class, dataProvider = "dp")
	public void validAccLogInTest(Hashtable<String,String>data) throws InterruptedException {
		
		SignUpPage sup = new SignUpPage();
		waitTillVisible("emailStringSUP_CSS");
		
		Thread.sleep(2000);
		
		sup.processLogIn(data.get("email"), data.get("password"));
		waitTillVisible("signOutBtn_CSS");
		
	}

}
