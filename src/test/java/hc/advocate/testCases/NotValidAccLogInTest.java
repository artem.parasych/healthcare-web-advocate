package hc.advocate.testCases;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import hc.advocate.pages.SignUpPage;
import hc.advocate.utilities.Utilities;

public class NotValidAccLogInTest extends BaseTest {
	
	@Test(dataProviderClass = Utilities.class, dataProvider = "dp")
	public void notValidAccLogInTest(Hashtable<String,String>data) throws IOException {
		
		SignUpPage sup = new SignUpPage();
		waitTillVisible("emailStringSUP_CSS");
		
		sup.processLogIn(data.get("email"), data.get("password"));
		isElementPresent("errorTextStringSUP_CSS");

		verifyEquals(data.get("assertText"), getText("errorTextStringSUP_CSS"));
		sup.tapOkErrorBtn();
		
		clear("emailStringSUP_CSS");
		clear("passwordStringSUP_CSS");
		
	}

}
